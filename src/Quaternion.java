import java.text.DecimalFormat;
import java.util.Objects;

public class Quaternion {

   private static final double DELTA = 0.000001;
   private double a;
   private double b;
   private double c;
   private double d;

   public Quaternion (double a, double b, double c, double d) {
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;
   }

   public double getRpart() {
      return a;
   }

   public double getIpart() {
      return b;
   }

   public double getJpart() {
      return c;
   }

   public double getKpart() {
      return d;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion:
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      String result = "";
      if (a != 0){
         result += firstDoubleToString(a);
      }
      if (b != 0){
         result += doubleToString(b) + "i";
      }
      if (c != 0){
         result += doubleToString(c) + "j";
      }
      if (d != 0){
         result += doubleToString(d) + "k";
      }
      return result;

   }

   public String doubleToString(double number){
      String result = "";
      if (number >= 0.){
         if (number % 1 == 0){
            result =  result + "+" + (int)number;
         } else{
            if (number % 1 < DELTA){
               result = result + "+" + (int)number;
            } else {
               result = result + "+" + number;
            }
         }
      } else{
         if (number % 1 == 0){
            result =  result + (int)number;
         } else {
            if (number % 1 < DELTA){
               result = result + (int)number;
            } else {
               result = result + (number%1);
            }
         }
      }
      return result;
   }

   public String firstDoubleToString(double number){
      DecimalFormat df = new DecimalFormat("#.####");
      String result = "";
      if (number >= 0.){
         if (number % 1 == 0){
            result =  result + (int)number;
         } else{
            if (number % 1 < DELTA){
               result = result + (int)number;
            } else {
               result = result + df.format(number);
            }
         }
      } else{
         if (number % 1 == 0){
            result =  result + (int)number;
         } else {
            if (number % 1 < DELTA){
               result = result + (int)number;
            } else {
               result = result + number;
            }
         }
      }
      return result;
   }

   /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      double a = 0d;
      double b = 0d;
      double c = 0d;
      double d = 0d;
      boolean canBeR = true;
      boolean canBeI = true;
      boolean canBeJ = true;
      boolean canBeK = true;
      boolean canBeOperator = true;
      StringBuilder number = new StringBuilder();

      char[] charArray = s.toCharArray();
      for (int i = 0; i < s.length(); i++) {
         if (i == 0){
            if (charArray[i] == '-'){
               number.append("-");
               canBeOperator = false;
            }else if (Character.isDigit(s.charAt(i))){
               if (i == (s.length() - 1) && canBeR){
                  number.append(charArray[i]);
                  a = Double.parseDouble(number.toString());
                  number = new StringBuilder();
                  canBeR = false;
               } else{
                  number.append(s.charAt(i));
                  canBeOperator = true;
               }
            } else if (!Character.isDigit(s.charAt(i))){
               throw new IllegalArgumentException("Character " + s.charAt(i) + " is not allowed in " + s);
            }
         } else{
            if (Character.isDigit(s.charAt(i))){
               if (i == (s.length() - 1) && canBeR){
                  number.append(charArray[i]);
                  a = Double.parseDouble(number.toString());
                  number = new StringBuilder();
                  canBeR = false;
               } else{
                  if (canBeR && canBeI && canBeJ && canBeK){
                     number.append(charArray[i]);
                     canBeOperator = true;
                  } else {
                     throw new IllegalArgumentException("Character " + s.charAt(i) + " is not allowed in " + s);
                  }
               }
            } else if (charArray[i] == 'i'){
               if (canBeI && canBeJ && canBeK){
                  b = Double.parseDouble(number.toString());
                  number = new StringBuilder();
                  canBeI = false;
                  if (canBeR){
                     canBeR = false;
                  }
               } else{
                  throw new IllegalArgumentException("Character " + s.charAt(i) + " is not allowed in " + s);
               }
            } else if (charArray[i] == 'j'){
               if (canBeJ && canBeK){
                  c = Double.parseDouble(number.toString());
                  number = new StringBuilder();
                  canBeJ = false;
                  if (canBeR){
                     canBeR = false;
                  }
               } else {
                  throw new IllegalArgumentException("Character " + s.charAt(i) + " is not allowed in " + s);
               }
            } else if (charArray[i] == 'k'){
               if (canBeK){
                  d = Double.parseDouble(number.toString());
                  number = new StringBuilder();
                  canBeK = false;
                  if (canBeR){
                     canBeR = false;
                  }
               } else {
                  throw new IllegalArgumentException("Character " + s.charAt(i) + " is not allowed in " + s);
               }
            } else if (charArray[i] == '-'){
               if (canBeR){
                  a = Double.parseDouble(number.toString());
                  number = new StringBuilder();
                  canBeR = false;
               }
               if (canBeOperator){
                  number.append("-");
                  canBeOperator = false;
               } else {
                  throw new IllegalArgumentException("Character " + s.charAt(i) + " is not allowed in " + s);
               }
            } else if (charArray[i] == '+'){
               if (canBeR){
                  a = Double.parseDouble(number.toString());
                  number = new StringBuilder();
                  canBeR = false;
               }
               if (canBeOperator){
                  canBeOperator = false;
               } else {
                  throw new IllegalArgumentException("Character " + s.charAt(i) + " is not allowed in " + s);
               }
            } else{
               throw new IllegalArgumentException("Character " + s.charAt(i) + " is not allowed in " + s);
            }
         }
      }
      return new Quaternion(a, b, c, d);
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(a,b,c,d);
   }

   /** Test whether the quaternion is zero.
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      if (Math.abs(a) < DELTA){
         if (Math.abs(b) < DELTA){
            if (Math.abs(c) < DELTA){
               if (Math.abs(b) < DELTA){
                  return true;
               }
            }
         }
      }
      return false;
   }

   /** Conjugate of the quaternion. Expressed by the formula
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      double nB = b * -1;
      double nC = c * -1;
      double nD = d * -1;
      return new Quaternion(a, nB, nC, nD);
   }

   /** Opposite of the quaternion. Expressed by the formula
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      double nA = a * -1;
      double nB = b * -1;
      double nC = c * -1;
      double nD = d * -1;
      return new Quaternion(nA,nB,nC,nD);
   }

   /** Sum of quaternions. Expressed by the formula
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      double a = this.a + q.getRpart();
      double b = this.b + q.getIpart();
      double c = this.c + q.getJpart();
      double d = this.d + q.getKpart();

      return new Quaternion(a,b,c,d);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      double a = ((this.a * q.getRpart()) - (this.b * q.getIpart()) - (this.c * q.getJpart()) - (this.d * q.getKpart()));
      double b = ((this.a * q.getIpart()) + (this.b * q.getRpart()) + (this.c * q.getKpart()) - (this.d * q.getJpart()));
      double c = ((this.a * q.getJpart()) - (this.b * q.getKpart()) + (this.c * q.getRpart()) + (this.d * q.getIpart()));
      double d = ((this.a * q.getKpart()) + (this.b * q.getJpart()) - (this.c * q.getIpart()) + (this.d * q.getRpart()));

      return new Quaternion(a,b,c,d);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      double a = this.a * r;
      double b = this.b * r;
      double c = this.c * r;
      double d = this.d * r;

      return new Quaternion(a,b,c,d);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if (isZero()){
         throw new ArithmeticException("Cannot divide by zero in 1/(a+bi+cj+dk) where a=" + a + ", b=" + b + ", c=" + c + ", d=" + d);
      }
      return new Quaternion(
              this.a/(this.a*this.a + this.b * this.b + this.c * this.c + this.d * this.d),
              (this.b * -1) /(this.a * this.a + this.b * this.b + this.c * this.c + this.d * this.d),
              (this.c * -1) /(this.a * this.a + this.b * this.b + this.c * this.c + this.d * this.d),
              (this.d * -1) /(this.a * this.a + this.b * this.b + this.c * this.c + this.d * this.d)
      );
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      double a = this.a - q.getRpart();
      double b = this.b - q.getIpart();
      double c = this.c - q.getJpart();
      double d = this.d - q.getKpart();

      return new Quaternion(a,b,c,d);
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if (q.isZero()){
         throw new ArithmeticException("Cannot divide by zero when inverting " + q);
      }
      return this.times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if (q.isZero()){
         throw new ArithmeticException("Cannot divide by zero when inverting " + q);
      }
      return q.inverse().times(this);
   }

   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if (qo instanceof Quaternion) {
         final Quaternion q = (Quaternion) qo;
         return this.minus(q).isZero();
      }
      return false;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      return (this.times(q.conjugate()).plus(q.times(this.conjugate()))).times(0.5);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(a,b,c,d);
   }

   /** Norm of the quaternion. Expressed by the formula
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(this.a * this.a + this.b * this.b + this.c * this.c + this.d * this.d);
   }

   /** Main method for testing purposes.
    * @param arg command line parameters
    */

   // "3+2i-4j-4k"
   public static void main (String[] arg) {
      Quaternion f = new Quaternion (-2., 2., 0., -4.);
      Quaternion test = Quaternion.valueOf("-1-2i-3j-4k");
      System.out.println(f.toString());
      System.out.println(Quaternion.valueOf (f.toString()));
        /*
        //System.out.println(test);
        Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
        if (arg.length > 0)
            arv1 = valueOf (arg[0]);
        System.out.println ("first: " + arv1.toString());
        System.out.println ("real: " + arv1.getRpart());
        System.out.println ("imagi: " + arv1.getIpart());
        System.out.println ("imagj: " + arv1.getJpart());
        System.out.println ("imagk: " + arv1.getKpart());
        System.out.println ("isZero: " + arv1.isZero());
        System.out.println ("conjugate: " + arv1.conjugate());
        System.out.println ("opposite: " + arv1.opposite());
        System.out.println ("hashCode: " + arv1.hashCode());
        Quaternion res = null;
        try {
            res = (Quaternion)arv1.clone();
        } catch (CloneNotSupportedException e) {};
        System.out.println ("clone equals to original: " + res.equals (arv1));
        System.out.println ("clone is not the same object: " + (res!=arv1));
        System.out.println ("hashCode: " + res.hashCode());
        res = valueOf (arv1.toString());
        System.out.println ("string conversion equals to original: "
                + res.equals (arv1));
        Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
        if (arg.length > 1)
            arv2 = valueOf (arg[1]);
        System.out.println ("second: " + arv2.toString());
        System.out.println ("hashCode: " + arv2.hashCode());
        System.out.println ("equals: " + arv1.equals (arv2));
        res = arv1.plus (arv2);
        System.out.println ("plus: " + res);
        System.out.println ("times: " + arv1.times (arv2));
        System.out.println ("minus: " + arv1.minus (arv2));
        double mm = arv1.norm();
        System.out.println ("norm: " + mm);
        System.out.println ("inverse: " + arv1.inverse());
        System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
        System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
        System.out.println ("dotMult: " + arv1.dotMult (arv2));

         */

   }
}
// end of file
